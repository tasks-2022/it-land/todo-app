import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export type AppErrorState = {
  errorCode: number;
};

const initialState: AppErrorState = {
  errorCode: 200,
};

export const appErrorSlice = createSlice({
  name: "appError",
  initialState,
  reducers: {
    setErrorCode(state, action: PayloadAction<number>) {
      state.errorCode = action.payload;
    },
  },
});
export const { setErrorCode } = appErrorSlice.actions;
export default appErrorSlice.reducer;
