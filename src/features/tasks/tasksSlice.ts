import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import moment from "moment";
import { TaskStatus, TaskType } from "../../components/task/types";

export type TaskState = {
  tasks: TaskType[];
  lastIndex: number;
};

const initialState: TaskState = {
  tasks: [
    {
      id: 1,
      title: "finishing first it land task",
      date: moment().format("MMMM Do YYYY h:mm a"),
      status: TaskStatus.Success,
    },
    {
      id: 2,
      title: "finishing second it land task",
      date: moment().format("MMMM Do YYYY h:mm a"),
      status: TaskStatus.Canceled,
    },
  ],
  lastIndex: 0,
};

export const taskSlice = createSlice({
  name: "tasks",
  initialState,
  reducers: {
    resetTasksState: () => initialState,
    createTask(state, action: PayloadAction<string>) {
      // check if the last index is defined .. if not generate new one
      if (state.lastIndex === 0 && state.tasks.length !== 0) {
        state.lastIndex =
          state.tasks.reduce((a, b) => (a.id > b.id ? a : b), {
            id: 0,
          }).id + 1;
      } else {
        state.lastIndex++;
      }
      // here we receive the task title only .. the rest of properties generate by code
      state.tasks.push({
        id: state.lastIndex,
        title: action.payload,
        date: moment().format("MMMM Do YYYY h:mm a"),
        status: TaskStatus.Pending,
      });
    },
    updateTask(state, action: PayloadAction<TaskType>) {
      const index = state.tasks.findIndex(
        (item) => item.id === action.payload.id
      );
      state.tasks.splice(index, 1, {
        ...action.payload,
      });
    },
    deleteTask(state, action: PayloadAction<number>) {
      // state.tasks = state.tasks.filter((tasks) => tasks.id !== action.payload);
      // faster deletion way since we delete one by one row
      state.tasks.splice(action.payload, 1);
    },
  },
});

export const { createTask, updateTask, deleteTask, resetTasksState } =
  taskSlice.actions;

export default taskSlice.reducer;
