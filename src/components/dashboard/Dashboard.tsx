import TaskCard from "../task/TaskCard";
import "./Dashboard.css";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../app/store";
import { TaskType } from "../task/types";
import { Button } from "antd";
import { PlusOutlined, ReloadOutlined } from "@ant-design/icons";
import { useState } from "react";
import {
  createTask,
  resetTasksState,
  updateTask,
} from "../../features/tasks/tasksSlice";
import TaskModal from "../task/TaskModal";

function Dashboard() {
  const dispatch = useDispatch();
  const { tasks } = useSelector((state: RootState) => state.tasks);
  const [isTaskModalVisible, setIsTaskModalVisible] = useState<boolean>(false);
  const [data, setData] = useState<TaskType | undefined>(undefined);

  const cards = tasks?.map((task: TaskType) => (
    <div key={task.id}>
      <TaskCard
        task={task}
        setIsTaskModalVisible={setIsTaskModalVisible}
        setData={setData}
      />
    </div>
  ));

  return (
    <div className="dashboard">
      <h1> TODO List </h1>

      <div
        style={{
          display: "grid",
          gridTemplateColumns: "2fr 4fr 2fr",
        }}
      >
        <div />
        <div className="todo-card">{cards}</div>
        <div />
      </div>
      <Button
        type="primary"
        className="add-todo-btn"
        icon={<PlusOutlined />}
        onClick={() => {
          setIsTaskModalVisible(true);
        }}
      />
      <Button
        type="primary"
        className="reset-todo-btn"
        icon={<ReloadOutlined />}
        onClick={() => {
          dispatch(resetTasksState());
        }}
        danger
      />
      <TaskModal
        handleSelect={(data) => {
          if (data.id === 0) dispatch(createTask(data.title));
          else dispatch(updateTask(data));
          setIsTaskModalVisible(false);
        }}
        visible={isTaskModalVisible}
        onCancel={() => setIsTaskModalVisible(false)}
        data={data}
        setData={setData}
      />
    </div>
  );
}
export default Dashboard;
