export type TaskType = {
  id: number;
  title: string;
  status: TaskStatus;
  date: string;
};

export enum TaskStatus {
  Success = "success",
  Canceled = "canceled",
  Pending = "pending", // default one
}
