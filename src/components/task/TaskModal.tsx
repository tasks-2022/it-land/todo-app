import { useEffect } from "react";
import { Button, Form, Input, message, Modal } from "antd";
import { TaskStatus, TaskType } from "./types";

interface ITaskModal {
  handleSelect: (record: TaskType) => void;
  visible: boolean;
  onCancel?: () => void;
  data: TaskType | undefined;
  setData: (record: TaskType | undefined) => void;
}

function TaskModal({
  handleSelect,
  visible,
  onCancel,
  data,
  setData,
}: ITaskModal) {
  const [form] = Form.useForm();

  // check if the required inputs got entered .. prevent save if not
  async function validateRecord() {
    /* this promise throws an error if field values are not valid and the following code will not be executed */
    // await the function before checking other fields
    if (
      !(await form
        .validateFields()
        .then()
        .catch(() => {
          message.error("Check your input");
          return false; // this is the return value of the promise
        }))
    )
      return false;
    return true;
  }

  // empty modal values after closing it ..
  useEffect(() => {
    form.resetFields();
  }, [form, visible]);

  return (
    <Modal
      visible={visible}
      onOk={() => {
        validateRecord().then((res) => {
          if (res) {
            if (data) handleSelect(data);
            setData(undefined);
          }
        });
      }}
      onCancel={() => {
        setData(undefined);
        if (onCancel) onCancel();
      }}
      footer={null}
      closable={false}
      width={600}
      destroyOnClose
      className="task-modal"
    >
      {/* TODO: move task from into separate TaskForm component */}
      <Form
        className="unitFormContainer"
        form={form}
        size="small"
        labelAlign="left"
        wrapperCol={{ span: 24 }}
        layout="horizontal"
        onKeyPress={(e) => {
          // handle entering data using enter clicking while user write in input
          if (e.key === "Enter" || e.key === "NumpadEnter") {
            validateRecord()
              .then((res) => {
                if (res) {
                  if (data) handleSelect(data);
                  // TODO: refactor type or value see what's better
                  return true;
                }
              })
              .catch((info) => {
                message.error("Check your input");
                return false;
              });
          }
        }}
        initialValues={data || undefined}
      >
        <Form.Item
          name="task"
          required
          rules={[{ required: true, message: "Plz enter a task." }]}
        >
          <div className="task-row">
            <Input
              placeholder="Type Any Task .. hint: you can use enter to save"
              autoFocus
              // autoSize={{ minRows: 1, maxRows: 2 }}
              onChange={(e) => {
                if (data?.title !== e.target.value)
                  // is there a way to avoid passing all the obj
                  setData({
                    id: 0,
                    status: TaskStatus.Pending,
                    date: "",
                    ...data,
                    title: e.target.value,
                  });
              }}
            />
            <div className="task-buttons">
              <Button
                danger
                type="primary"
                onClick={() => {
                  if (onCancel) onCancel();
                  setData(undefined);
                }}
              >
                Cancel
              </Button>
              <Button
                type="primary"
                onClick={() => {
                  validateRecord().then((res) => {
                    if (res) {
                      if (data) handleSelect(data);
                      if (onCancel) onCancel();
                      setData(undefined);
                    }
                  });
                }}
              >
                Done
              </Button>
            </div>
          </div>
        </Form.Item>
      </Form>
    </Modal>
  );
}
TaskModal.defaultProps = {
  onCancel: undefined,
};

export default TaskModal;
