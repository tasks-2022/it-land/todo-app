import {
  CheckOutlined,
  CloseOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { Button } from "antd";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { deleteTask, updateTask } from "../../features/tasks/tasksSlice";
import { TaskStatus, TaskType } from "./types";
import "./Task.css";

interface ITaskProp {
  task: TaskType;
  setIsTaskModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  setData: React.Dispatch<React.SetStateAction<TaskType | undefined>>;
}

function TaskCard({ task, setIsTaskModalVisible, setData }: ITaskProp) {
  const dispatch = useDispatch();

  const onSuccess = useCallback(
    (task: TaskType) => {
      dispatch(updateTask({ ...task, status: TaskStatus.Success }));
    },
    [dispatch]
  );

  const onCancel = useCallback(
    (task: TaskType) => {
      dispatch(updateTask({ ...task, status: TaskStatus.Canceled }));
    },
    [dispatch]
  );

  const onEdit = useCallback(
    (task: TaskType) => {
      setIsTaskModalVisible(true);
      setData(task); // select the target task to be passed to updateTask action
    },
    [setData, setIsTaskModalVisible]
  );

  const onDelete = useCallback(
    (taskID: number) => {
      dispatch(deleteTask(taskID));
    },
    [dispatch]
  );

  return (
    <>
      <div
        // dynamic styling base on task status
        className={`task-card-${task.status}`}
      >
        <div className="task-details">
          <span className="task-date">{task.date}</span>
          <br />
          <div className="task-title">{task.title}</div>
        </div>
        <div className="editing-group-buttons">
          <Button
            type={task.status === TaskStatus.Pending ? "primary" : "default"}
            shape="circle"
            icon={<DeleteOutlined />}
            danger
            onClick={() => onDelete(task.id)}
          />
          {task.status === TaskStatus.Pending && (
            <>
              <Button
                type="primary"
                shape="circle"
                icon={<EditOutlined />}
                onClick={() => onEdit(task)}
              />
              <Button
                type="primary"
                shape="circle"
                icon={<CloseOutlined />}
                danger
                onClick={() => onCancel(task)}
              />
              <Button
                className="success-card-button"
                shape="circle"
                icon={<CheckOutlined />}
                onClick={() => onSuccess(task)}
              />
            </>
          )}
        </div>
      </div>
    </>
  );
}
export default TaskCard;
