import "./App.css";
import Dashboard from "./components/dashboard/Dashboard";

function App() {
  return (
    <div className="App">
      <div className="control-btn-panel" />
      <Dashboard />
    </div>
  );
}

export default App;
