import { API_BASE_URL, DATA_URL } from "./Constants";
import request from "./Request";

export function getData() {
  return request({
    url: API_BASE_URL + DATA_URL, // "https://jsonplaceholder.typicode.com/photos", // API_BASE_URL + DATA_URL,
    method: "GET",
  });
}
