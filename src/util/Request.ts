import axios from "axios";

import { ReqOptionsType } from "../types/APITypes";
import { setErrorCode } from "../features/error/appErrorSlice";
import { store } from "../app/store";

const request = (options: ReqOptionsType) => {
  const headers = {
    "Content-Type": "application/json",
  };
  return axios({ ...options, headers })
    .then((response) => {
      store.dispatch(setErrorCode(200));
      return response.data;
    })
    .catch((error) => {
      console.log(`axios error:${error}`);
      if (error.response) store.dispatch(setErrorCode(error.response.status));
    });
};
export default request;
