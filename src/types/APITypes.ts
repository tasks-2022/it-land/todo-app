export type ReqOptionsType = {
  url: string;
  method: "GET" | "POST" | "PUT";
  data?: any | undefined;
  noAuth?: boolean;
  mode?: "cors" | "no-cors";
};

export type APIResponseType = {
  status: string;
  data?: any | null;
  error?: string;
};